﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MusicStore.Models;

namespace MusicStore.Data
{
    public static class DbInitializer
    {
        public static void Initialize(MusicStoreContext context)
        {
            context.Database.EnsureCreated();

            //Look for any album
            if (context.Albums.Any())
            {
                return;
            }

            var albums = new Album[]
            {
            new Album{Name="GetMoney",ArtistName="Au$tin",Price=10,Genre="Rap" }
            };
            foreach (Album a in albums)
            {
                context.Albums.Add(a);
            }
            context.SaveChanges();
        }
    }
}
